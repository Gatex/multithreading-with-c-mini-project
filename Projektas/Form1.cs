﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projektas
{
    public partial class Form1 : Form
    {
        

        private string dataFile;
        private const int maxWords = 10000;
        private string[] array = new string[maxWords];
        private string[] sortedArray = new string[maxWords];
        private int wordCount = 0;
        private int sortedCount = 0;
        private int threadCount = 0;

        private string[,] tWords = new string[8, 5000];
        private int wordsPerThread = 0;

        private Object thisLock = new Object();

        private bool readDone = false;

        Regex rgx = new Regex("[^a-zA-Z -]");

        DateTime startSingleTime;
        DateTime startParallelTime;
        TimeSpan singleTime;
        TimeSpan parallelTime;
        double seconds;

        delegate void SetTextCallback(Control ctrl, string text);
        delegate void SetEnabledCallback(Control ctrl, bool state);


        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Duomenų nuskaitymo metodas
        /// </summary>
        /// <param name="file">Kelias iki duomenų failo</param>
        private void readData(string file)
        {
            wordCount = 0;
            using (StreamReader sr = new StreamReader(file))
            {
                while (sr.Peek() >= 0)
                {
                    string line = sr.ReadLine();
                    line = rgx.Replace(line, "");
                    string[] words = line.Split();
                    foreach (string word in words)
                    {
                        if (word != "")
                        {
                            array[wordCount] = word;
                            wordCount++;
                        }
                    }
                }
                readDone = true;
                sr.Close();
            }
        }

        /// <summary>
        /// Duomenų į bendrą surikiuotą masyvą pridėjimo metodas
        /// </summary>
        /// <param name="words">Pridedami duomenys</param>
        private void addSortedData(string words)
        {
                for (int i = 0; i < sortedCount + 1; i++)
                {
                    if (sortedArray[i] == null)
                    {
                        sortedArray[i] = words;
                        sortedCount++;
                        break;
                    }
                    else if (sortedArray[i].CompareTo(words) > 0 || sortedArray[i].CompareTo(words) == 0)
                    {
                        int n = sortedCount;
                        while (n > i)
                        {
                            sortedArray[n] = sortedArray[n - 1];
                            n--;
                        }
                        sortedArray[i] = words;
                        sortedCount++;
                        break;
                    }
                }
        }

        /// <summary>
        /// Informacijos apie gijas išvedimo į GUI metodas
        /// </summary>
        /// <returns></returns>
        private bool countThreads()
        {
            string label = "";
            if (comboBox1.Text != "")
            {
                threadCount = int.Parse(comboBox1.Text);
                    if (threadCount > 1 && threadCount < 10)
                    {
                        label = "gijos";
                    }
                    else if (threadCount % 10 == 0 || (threadCount > 10 && threadCount < 20) || (threadCount % 100 > 10 && threadCount % 100 < 20))
                    {
                        label = "gijų";
                    }
                    else if (threadCount % 10 == 1)
                    {
                        label = "gija";
                    }
                    else
                    {
                        label = "gijos";
                    }
                    label13.Text = threadCount.ToString() + " " + label;
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Duomenų spausdinimo į GUI metodas
        /// </summary>
        private void printData()
        {
            for (int i = 0; i < wordCount; i++)
            {
                appendText(textBox1, array[i] + Environment.NewLine);
            }
            appendText(textBox8, DateTime.Now.ToLongTimeString() + ": Duomenų spausdinimas baigtas!" + Environment.NewLine);
            changeEnabled(label1, true);
            changeEnabled(label14, true);
            changeEnabled(button4, true);
            changeEnabled(button5, true);
            changeEnabled(comboBox1, true);
        }

        /// <summary>
        /// Ne paralelaus vykdymo rezultatų spausdinimo į GUI metodas
        /// </summary>
        private void printSingleResults()
        {
                for (int j = 0; j < sortedCount; j++)
                {
                    appendText(textBox2, sortedArray[j] + Environment.NewLine);
                }
                appendText(textBox8, DateTime.Now.ToLongTimeString() + ": Ne paralelus rezultatų spausdinimas baigtas!" + Environment.NewLine);
                singleTime = DateTime.Now - startSingleTime;
                seconds = singleTime.TotalMilliseconds / 1000;
                setText(textBox5, seconds.ToString());
        }

        /// <summary>
        /// Paralelaus vykdymmo rezultatų spausdinimo į GUI metodas
        /// </summary>
        private void printParallelResults()
        {
            for (int j = 0; j < sortedCount; j++)
            {
                if (sortedArray[j] != null)
                {
                    appendText(textBox6, sortedArray[j] + Environment.NewLine);
                }
            }
            appendText(textBox8, DateTime.Now.ToLongTimeString() + ": Paralelus rezultatų spausdinimas baigtas!" + Environment.NewLine);
            parallelTime = DateTime.Now - startParallelTime;
            seconds = parallelTime.TotalMilliseconds / 1000;
            setText(textBox7, seconds.ToString());
        }

        /// <summary>
        /// Žodžių nustatymo vienam procesui metodas
        /// </summary>
        private void setWords()
        {
            if(threadCount == 2)
            {
                wordsPerThread = wordCount / 2 + wordCount % 2;
                tWords = new string[2, wordsPerThread];
                for (int i = 0; i < wordsPerThread; i++)
                {
                    tWords[0, i] = array[i];
                }
                int index = 0;
                for (int i = wordsPerThread; i < wordCount; i++)
                {
                    tWords[1, index] = array[i];
                    index++;
                }
            }
            else if (threadCount == 4)
            {
                wordsPerThread = wordCount / 4 + wordCount % 4;
                tWords = new string[4, wordsPerThread];
                int index = 0;
                for (int i = 0; i < wordsPerThread; i++)
                {
                    tWords[0, i] = array[i];
                }
                index = 0;
                for (int i = wordsPerThread; i < 2*wordsPerThread; i++)
                {
                    tWords[1, index] = array[i];
                    index++;
                }
                index = 0;
                for (int i = 2*wordsPerThread; i < 3*wordsPerThread; i++)
                {
                    tWords[2, index] = array[i];
                    index++;
                }
                index = 0;
                for (int i = 3 * wordsPerThread; i < wordCount; i++)
                {
                    tWords[3, index] = array[i];
                    index++;
                }
            }
            else if (threadCount == 8)
            {
                wordsPerThread = wordCount / 8 + wordCount % 8;
                tWords = new string[8, wordsPerThread];
                int index = 0;
                for (int i = 0; i < wordsPerThread; i++)
                {
                    tWords[0, i] = array[i];
                }
                index = 0;
                for (int i = wordsPerThread; i < 2 * wordsPerThread; i++)
                {
                    tWords[1, index] = array[i];
                    index++;
                }
                index = 0;
                for (int i = 2 * wordsPerThread; i < 3 * wordsPerThread; i++)
                {
                    tWords[2, index] = array[i];
                    index++;
                }
                index = 0;
                for (int i = 3 * wordsPerThread; i < 4 * wordsPerThread; i++)
                {
                    tWords[3, index] = array[i];
                    index++;
                }
                index = 0;
                for (int i = 4 * wordsPerThread; i < 5 * wordsPerThread; i++)
                {
                    tWords[4, index] = array[i];
                }
                index = 0;
                for (int i = 5* wordsPerThread; i < 6 * wordsPerThread; i++)
                {
                    tWords[5, index] = array[i];
                    index++;
                }
                index = 0;
                for (int i = 6 * wordsPerThread; i < 7 * wordsPerThread; i++)
                {
                    tWords[6, index] = array[i];
                    index++;
                }
                index = 0;
                for (int i = 7 * wordsPerThread; i < wordCount; i++)
                {
                    tWords[7, index] = array[i];
                    index++;
                }
            }
        }

        /// <summary>
        /// GUI atributų teksto keitimo paraleliai metodas
        /// </summary>
        /// <param name="ctrl">GUI atributas</param>
        /// <param name="text">Tekstas, į kurį bus keičiama</param>
        private void setText(Control ctrl, string text)
        {
            if (ctrl.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(setText);
                this.Invoke(d, new object[] { ctrl, text });
            }
            else
            {
                ctrl.Text = text;
            }
        }

        /// <summary>
        /// GUI atributų teksto pridėjimo paraleliai metodas
        /// </summary>
        /// <param name="ctrl">GUI atributas</param>
        /// <param name="text">Tekstas, kuris bus pridedamas</param>
        private void appendText(Control ctrl, string text)
        {
            if (ctrl.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(appendText);
                this.Invoke(d, new object[] {ctrl, text });
            }
            else
            {
                ctrl.Text += text;
            }
        }

        /// <summary>
        /// GUI atributų Enabled būsenos keitimo paraleliai metodas
        /// </summary>
        /// <param name="ctrl">GUI atributas</param>
        /// <param name="state">Būsena, į kurią bus keičiama</param>
        private void changeEnabled(Control ctrl, bool state)
        {
            if (ctrl.InvokeRequired)
            {
                SetEnabledCallback e = new SetEnabledCallback(changeEnabled);
                this.Invoke(e, new object[] { ctrl, state });
            }
            else
            {
                ctrl.Enabled = state;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            array = new string[maxWords];
            wordCount = 0;
            var readTask = Task.Factory.StartNew(() => readData(dataFile));
            textBox3.Enabled = true;
            label3.Enabled = true;
            while (!readDone)
            {
                appendText(textBox8, DateTime.Now.ToLongTimeString() + ": Laukiama, kol bus baigtas nuskaitymas!" + Environment.NewLine);
            }
            appendText(textBox8, DateTime.Now.ToLongTimeString() + ": Duomenų nuskaitymas baigtas!" + Environment.NewLine);
            appendText(textBox8, DateTime.Now.ToLongTimeString() + ": Duomenų spausdinimas pradėtas!" + Environment.NewLine);
            var printTask = Task.Factory.StartNew(() => printData());
            textBox3.Text = wordCount.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            array = new string[maxWords];
            sortedArray = new string[maxWords];
            wordCount = 0;
            sortedCount = 0;

            button1.Enabled = false;
            label1.Enabled = false;
            label3.Enabled = false;
            label14.Enabled = false;
            button4.Enabled = false;
            button5.Enabled = false;
            comboBox1.Enabled = false;
            textBox3.Enabled = false;
            textBox8.Clear();
            textBox5.Clear();


            openFileDialog1.ShowDialog();
            if (!openFileDialog1.FileName.Contains(".txt"))
            {
                button3.Text = "Blogas failas!";
                button3.BackColor = Color.Red;
            }
            else if (openFileDialog1.FileName != "")
            {
                button3.Text = "Failas pasirinktas!";
                dataFile = openFileDialog1.FileName;
                button3.BackColor = Color.LawnGreen;
                button1.Enabled = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            sortedArray = new string[maxWords];
            sortedCount = 0;
            textBox2.Clear();
            startSingleTime = DateTime.Now;            
            addSingleSortData(array);
            appendText(textBox8, DateTime.Now.ToLongTimeString() + ": Ne paralelus rezultatų spausdinimas pradėtas!" + Environment.NewLine);

            var printTask = Task.Factory.StartNew(() => printSingleResults());
        }

        private void addSingleSortData(string[] words)
        {
            for (int i = 0; i < wordCount; i++)
            {
                    addSortedData(words[i]);
            }

        }

        private void addParallelSortData(string[,] words, int nr)
        {
            for (int i = 0; i < wordsPerThread; i++)
            {
                lock (thisLock)
                {
                   addSortedData(words[nr, i]);
                }
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            Task task1;
            Task task2;
            Task task3;
            Task task4;
            Task task5;
            Task task6;
            Task task7;
            Task task8;
            readData(dataFile);
            sortedArray = new string[maxWords];
            sortedCount = 0;
            textBox6.Clear();
            if (countThreads())
            {
                setWords();
                appendText(textBox8, DateTime.Now.ToLongTimeString() + ": Paralelus rezultatų spausdinimas pradėtas!" + Environment.NewLine);
                startParallelTime = DateTime.Now;
                switch (threadCount)
                {
                    case 2:
                        task1 = Task.Factory.StartNew(() => addParallelSortData(tWords, 0));
                        task2 = Task.Factory.StartNew(() => addParallelSortData(tWords, 1));
                        Task[] tasks2 = { task1, task2 };
                        Task.WaitAll(tasks2);
                        break;
                    case 4:
                        task1 = Task.Factory.StartNew(() => addParallelSortData(tWords, 0));
                        task2 = Task.Factory.StartNew(() => addParallelSortData(tWords, 1));
                        task3 = Task.Factory.StartNew(() => addParallelSortData(tWords, 2));
                        task4 = Task.Factory.StartNew(() => addParallelSortData(tWords, 3));
                        Task[] tasks4 = { task1, task2, task3, task4 };
                        Task.WaitAll(tasks4);
                        break;
                    case 8:
                        task1 = Task.Factory.StartNew(() => addParallelSortData(tWords, 0));
                        task2 = Task.Factory.StartNew(() => addParallelSortData(tWords, 1));
                        task3 = Task.Factory.StartNew(() => addParallelSortData(tWords, 2));
                        task4 = Task.Factory.StartNew(() => addParallelSortData(tWords, 3));
                        task5 = Task.Factory.StartNew(() => addParallelSortData(tWords, 4));
                        task6 = Task.Factory.StartNew(() => addParallelSortData(tWords, 5));
                        task7 = Task.Factory.StartNew(() => addParallelSortData(tWords, 6));
                        task8 = Task.Factory.StartNew(() => addParallelSortData(tWords, 7));
                        Task[] tasks8 = { task1, task2, task3, task4, task5, task6, task7, task8 };
                        Task.WaitAll(tasks8);
                        break;
                }
                Task.Factory.StartNew(() => printParallelResults());
            }
        }
    }
}
