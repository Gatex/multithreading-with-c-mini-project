##ABOUT 
  
***This is an university project which was created in order to learn multithreading in C# fundamentals.***  
***It is a simple Windows form application with even more simple GUI.***  
  
***Features:***  
	-User can choose a text file from his computer via file browser's interface  
	-The program then reads all words from a file and prints them out to the interface  
	-Then user can choose sequential or parallel (2, 4 or 8 threads) methods to print those words in alphabetical order  
	-GUI also has log windows where program's status and messages about actions are written  
	-Program also counts processing time for each action so user can evaluate differences between sequential and parallel (with different amount of threads) operations in terms of efficiency

